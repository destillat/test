const path = require('path');

module.exports = {
  paths: {
    source: path.resolve(__dirname, '../src/'),
    public: path.resolve(__dirname, '../public'),
    output: path.resolve(__dirname, '../dist/'),
  },
  server: {
    host: 'localhost',
    port: 8000,
  },
};
