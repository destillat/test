import { Data, DataSource, Observer } from "../../DataSource/DataSource";
import { Option, renderSelectInput } from "./Inputs/SelectInput";
import { uniq } from 'lodash';

const Inputs = {
    select: renderSelectInput,
} as const;

interface Filter {
    label: string;
    code: string;
    type: keyof typeof Inputs;
    defaultValue: unknown;
}

interface DataSourceFilterWidgetConfiguration {
    filters: Filter[],
}

export const createDataSourceFilterWidget = (
    element: Element,
    configuration: DataSourceFilterWidgetConfiguration,
    dataSource: DataSource,
): DataSource => {
    let filters = configuration.filters.map(filter => ({
        ...filter,
        value: filter.defaultValue,
    }));
    let lastData: Data = [];
    const observerMap = new Map<symbol, Observer>();

    const applyFilters = (data: Data): Data => {
        return data.filter(values => {
            for (const filter of filters) {
                if (filter.value !== '') {
                    const colIndex = dataSource.columns.findIndex(col => col.code === filter.code);
                    if (values[colIndex] !== filter.value) {
                        return false;
                    }
                }
            }
            return true;
        });
    }    
    
    const subscribe = (observer: Observer) => {
        observer.onData(applyFilters(lastData));
        
        const id = Symbol();
        observerMap.set(id, observer);

        const unsubscribe = () => {
            observerMap.delete(id);
        };

        return {
            unsubscribe,
        }
    }

    const render = (data: Data) => {
        element.textContent = '';
        for (const filter of filters) {
            const colIndex = dataSource.columns.findIndex(col => col.code === filter.code);
            const values = uniq(data.map(values => values[colIndex])) as string[];
            
            const label = document.createElement('label');
            label.textContent = filter.label;
            element.appendChild(label)

            switch (filter.type) {
                case 'select': {
                    const options = [
                        {
                            value: '',
                            label: 'Всe',
                        },
                        ...(values as string[]).map<Option>(value => ({
                            label: value,
                            value: value,
                        }))
                    ];
                    renderSelectInput(
                        element,
                        options,
                        filter.value as string,
                        (option?: Option) => {
                            filter.value = option?.value ?? ''
                            console.log(filter);
                            update(data)
                        },
                    );
                    break;
                }
            }            
        }
    }

    const update = (newData: Data) => {
        lastData = newData;
        render(newData);
        observerMap.forEach(observer => {
            observer.onData(applyFilters(newData));
        })
    }

    dataSource.subscribe(({
        onData: update,
    }));

    return {
        columns: dataSource.columns,
        subscribe,
    }
}