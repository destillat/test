import { toString } from "lodash";

export interface Option {
    value: string;
    label: string;
}

export const renderSelectInput = (element: Element, options: Option[], defaultValue: string, onChange: (option?: Option) => void) => {
    const select = element.appendChild(document.createElement('select'));
    const optionsMap = new Map<string, Option>();
    select.onchange = () => {
        const selectedOption = select.options[select.selectedIndex];
        const selectedOptionValue = selectedOption.value;
        onChange(optionsMap.get(selectedOptionValue))
    };
    select.value = defaultValue;
    for (const option of options) {
        const { label, value } = option;
        optionsMap.set(toString(value), option);
        const optionElement = select.appendChild(document.createElement('option'));
        optionElement.value = value;
        optionElement.text = label;
        optionElement.selected = value === defaultValue;
    }
}