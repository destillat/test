import { identity, toString} from "lodash";
import { Data, DataSource } from "../../DataSource/DataSource";

const Columns = {
    string: (value: unknown, row: unknown, rowIndex: number) => {
        const element = document.createElement('span');
        element.textContent = toString(value);
        return element;
    }
} as const;

interface Column {
    header: string;
    code: string;
    formatter?: (value: unknown, row: unknown, rowIndex: number) => unknown,
    renderer?: keyof typeof Columns;
}

interface DataSourceViewWidgetConfiguration {
    columns: Column[]
}

export const createDataSourceViewWidget = (
    element: Element,
    configuration: DataSourceViewWidgetConfiguration,
    dataSource: DataSource,
) => {
    const renderHeader = () => {
        const headerRow = document.createElement('tr');
        for (const column of configuration.columns) {
            const headerColumn = document.createElement('th');
            headerColumn.textContent = column.header;
            headerRow.appendChild(headerColumn);
        }
        return headerRow;
    }

    const renderBody = (data: Data) => {
        const tableBodyElement = document.createElement('tbody');
        for (const [index, dataRow] of data.entries()) {

            const rowElement = document.createElement('tr');
            for (const column of configuration.columns) {

                const columnElement = document.createElement('td');
                const colIndex =  dataSource.columns.findIndex(col => col.code === column.code);
                const value = dataRow[colIndex];
                const formattedValue = column.formatter ? column.formatter(value, dataRow, index) : value;
                const renderer = Columns[column.renderer ?? 'string'];
                const renderedValue = renderer(formattedValue, dataRow, index);
                columnElement.appendChild(renderedValue);

                rowElement.appendChild(columnElement);
            }

            tableBodyElement.appendChild(rowElement)
        }

        return tableBodyElement;
    }

    const render = (data: Data) => {
        element.textContent = '';

        const table = document.createElement('table');
        const header = renderHeader();
        table.appendChild(header);
        const body = renderBody(data);
        table.appendChild(body);

        element.appendChild(table);
    }

    dataSource.subscribe({
        onData: render,
    })
}