import { createDataSourceFilterWidget } from '../Widget/Filter/DataSourceFilterWidget';
import { getJSONObjectDataSource } from '../DataSource/JSONObjectDataSource';
import { createDataSourceViewWidget } from '../Widget/View/DataSourceViewWidget';
import data from './__fixtures__/data.json';
import moment from 'moment';


const jsonDataSource = getJSONObjectDataSource(data);

const rootElement = document.querySelector('#root');

const defaultColumns = [
    {
        code: 'id',
        header: 'ID',
    },
    {
        code: 'name',
        header: 'Название',
    },
    {
        code: 'category',
        header: 'Категория',
    },
    {
        code: 'date',
        header: 'Дата',
        formatter: (value: unknown) => moment(value as number).format()
    },
    {
        code: 'value',
        header: 'Значение'
    }
];

if (rootElement) {

    const createFilter = () => rootElement.appendChild(document.createElement('div'));
    const createView = () => rootElement.appendChild(document.createElement('div'));
    const createHeader = (title: string) => {
        const h3 = document.createElement('h3');
        h3.textContent = title;
        rootElement.appendChild(h3);
    }

    createHeader('Not filtered');

    createDataSourceViewWidget(
        createView(),
        {
            columns: defaultColumns
        },
        jsonDataSource,
    );

    createHeader('Filtered multiple tables');
    const filteredDataSource = createDataSourceFilterWidget(
        createFilter(),
        {
            filters: [
                {
                    label: 'id',
                    code: 'id',
                    type: 'select',
                    defaultValue: '',
                },
                {
                    label: 'name',
                    code: 'name',
                    type: 'select',
                    defaultValue: '',
                },
                {
                    label: 'category',
                    code: 'category',
                    type: 'select',
                    defaultValue: '',
                },
                {
                    label: 'date',
                    code: 'date',
                    type: 'select',
                    defaultValue: '',
                },
                {
                    label: 'value',
                    code: 'value',
                    type: 'select',
                    defaultValue: '',
                }
            ],
        },
        jsonDataSource
    );
    
    createDataSourceViewWidget(
        createView(),
        {
            columns: defaultColumns,
        },
        filteredDataSource,
    );

    createDataSourceViewWidget(
        createView(),
        {
            columns: defaultColumns
        },
        filteredDataSource,
    );

    createHeader('Second filter')

    const secondDataSource = createDataSourceFilterWidget(
        createFilter(),
        {
            filters: [
                {
                    label: 'test',
                    code: 'id',
                    type: 'select',
                    defaultValue: '',
                }
            ],
        },
        jsonDataSource
    );

    createDataSourceViewWidget(
        createView(),
        {
            columns: defaultColumns
        },
        secondDataSource,
    );
}
