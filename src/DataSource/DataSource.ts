export interface Observer {
    onData: (data: Data) => void;
}

export interface DataSource {
    columns: Column[];
    subscribe: (observer: Observer) => ({
        unsubscribe: () => void
    });
}

export interface Column {
    code: string;
    label: string;
    type: "string" | "unixtimestamp" | "number";
}

export interface Data extends Array<Array<unknown>> {}
