import { DataSource, Observer } from "./DataSource";
import { noop } from 'lodash';

export interface JSONObjectDataSource extends DataSource {

}

export const getJSONObjectDataSource = (data: any): JSONObjectDataSource => {
    const subscribe = (observer: Observer) => {
        observer.onData(data.data);

        return {
            unsubscribe: noop,
        }
    }
    return {
        columns: data.columns,
        subscribe,
    }
}